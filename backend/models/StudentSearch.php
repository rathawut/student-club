<?php

namespace backend\models;

use common\models\StudentQuery;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Student;

/**
 * StudentSearch represents the model behind the search form about `common\models\Student`.
 */
class StudentSearch extends Student
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'school_grade_id', 'academic_year_id', 'club_id'], 'integer'],
            [['identification_number', 'title_name', 'first_name', 'last_name', 'gender', 'address', 'phone_number', 'nationality', 'race', 'religion', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param StudentQuery $query
     *
     * @return ActiveDataProvider
     */
    public function search($params, $query = null)
    {
        if (!$query) {
            $query = Student::find();
        }
        $query->orderBy(['active' => SORT_ASC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'school_grade_id' => $this->school_grade_id,
            'academic_year_id' => $this->academic_year_id,
            'club_id' => $this->club_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'identification_number', $this->identification_number])
            ->andFilterWhere(['like', 'title_name', $this->title_name])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'gender', $this->gender])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'nationality', $this->nationality])
            ->andFilterWhere(['like', 'race', $this->race])
            ->andFilterWhere(['like', 'religion', $this->religion]);

        return $dataProvider;
    }
}
