<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Club;

/**
 * ClubSearch represents the model behind the search form about `common\models\Club`.
 */
class ClubSearch extends Club
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'academic_year_id', 'quota', 'teacher_advisor_id', 'student_leader_id', 'created_by', 'updated_by'], 'integer'],
            [['name', 'joining_start_at', 'joining_end_at', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @param $query
     * @return ActiveDataProvider
     */
    public function search($params, $query)
    {
        if (!$query) {
            $query = Club::find();
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'academic_year_id' => $this->academic_year_id,
            'quota' => $this->quota,
            'joining_start_at' => $this->joining_start_at,
            'joining_end_at' => $this->joining_end_at,
            'teacher_advisor_id' => $this->teacher_advisor_id,
            'student_leader_id' => $this->student_leader_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
