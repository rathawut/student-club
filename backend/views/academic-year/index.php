<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\AcademicYearSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ปีการศึกษา';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-year-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'สร้างปีการศึกษา', [
            'modelClass' => 'Academic Year',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin() ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'year',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    return $model->active
                        ? "<span style='color: green'>กำลังเปิดรับสมัคร</span>"
                        : "<span style='color: red'>ปิดรับสมัครแล้ว</span>";
                },
                'format' => 'html',
            ],
            [
                'class' => '\yii2mod\toggle\ToggleColumn',
                'attribute' => 'active',
            ],
//            'created_at',
//            'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
