<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\AcademicYear */

$this->title = Yii::t('backend', 'สร้าง {modelClass}', [
    'modelClass' => 'Academic Year',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Academic Years'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="academic-year-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
