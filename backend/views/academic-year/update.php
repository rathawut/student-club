<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AcademicYear */

$this->title = Yii::t('backend', 'แก้ไข {modelClass}: ', [
    'modelClass' => 'Academic Year',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Academic Years'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="academic-year-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
