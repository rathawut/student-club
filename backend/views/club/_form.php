<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Club */
/* @var $form yii\bootstrap\ActiveForm */

$academicYears = ['' => 'กรุณาเลือก ...'] + ArrayHelper::map(\common\models\AcademicYear::find()->all(), 'id', 'year');
/** @var \common\models\User[] $users */
$users = \common\models\User::find()->with('userProfile')->all();
$teachers = [];
foreach ($users as $user) {
    if (Yii::$app->authManager->checkAccess($user->id, 'teacher')) {
        $teachers[$user->id] = $user->username;
    }
}
$students = ArrayHelper::map(\common\models\Student::find()->all(), 'id', 'fullName');
?>

<div class="club-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php echo $form->field($model, 'academic_year_id')->dropDownList($academicYears) ?>

    <?php echo $form->field($model, 'quota')->textInput() ?>

    <?php echo $form->field($model, 'establishment_date')->widget(\kartik\date\DatePicker::class, [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
        ],
    ]) ?>

    <?php echo $form->field($model, 'joining_start_at')->widget(\kartik\datetime\DateTimePicker::class, [
        'pluginOptions' => [
            'autoclose' => true,
        ],
    ]) ?>

    <?php echo $form->field($model, 'joining_end_at')->widget(\kartik\datetime\DateTimePicker::class, [
        'pluginOptions' => [
            'autoclose' => true,
        ],
    ]) ?>

    <?php echo $form->field($model, 'teacher_advisor_id')->dropDownList($teachers, ['prompt' => 'กรุณาเลือก ...']) ?>

    <?php echo $form->field($model, 'student_leader_id')->dropDownList($students, ['prompt' => 'กรุณาเลือก ...']) ?>

    <?php echo $form->field($model, 'school_grade_ids')->checkboxList(ArrayHelper::map(\common\models\SchoolGrade::find()->asArray()->all(), 'id', 'grade')) ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Create') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
