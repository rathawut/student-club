<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Club */

$this->title = Yii::t('backend', 'แก้ไข {modelClass}: ', [
    'modelClass' => 'Club',
]) . ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Clubs'), 'url' => ['index', 'year' => $model->academicYear->year]];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="club-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
