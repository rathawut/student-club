<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Club */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Clubs'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$studentCount = count($model->students);
$students = $model->students;
ArrayHelper::multisort($students, ['active'], [SORT_DESC]);
?>
<!-- Custom Tabs -->
<div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">ข้อมูลชุมนุม</a>
        </li>
        <li class=""><a href="#tab_2" data-toggle="tab"
                        aria-expanded="true">สมาชิกชุมนุม</a></li>
        <li class=""><a href="#tab_3" data-toggle="tab"
                        aria-expanded="false">กิจกรรมชุมนุม</a></li>
    </ul>
    <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">ข้อมูลชุมนุม</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <?php echo DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'academicYear.year',
                            'quota',
                            'joining_start_at',
                            'joining_end_at',
                            [
                                'label' => 'ที่ปรึกษาชุมนุม',
                                'value' => function ($model) {
                                    return $model->teacherAdvisor->username;
                                },
                            ],
                            [
                                'label' => 'ชื่อประธานชุมนุม',
                                'value' => function ($model) {
                                    return $model->studentLeader->fullName;
                                },
                            ],
                        ],
                    ]) ?>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">สมาชิกชุมนุม</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-box-tool" onclick="window.print()">
                            <i class="fa fa-print"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <h3 class="text-center"><?= "สมาชิก {$studentCount} คน" ?></h3>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>ระดับชั้น</th>
                            <th>สถานะ</th>
                            <th>คะแนนรวม</th>
                            <th>คะแนนเต็ม</th>
                            <th>การผ่านเกณฑ์</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($students as $index => $student) {
                            $allScore = count($model->activities) * 10;
                            $baselineScore = $allScore / 2;
                            echo "<tr>";
                            echo "<td>" . ++$index . "</td>";
                            echo "<td>{$student->fullName}</td>";
                            echo "<td style='color: orange'>ชั้น{$student->schoolGrade->grade}</td>";
                            if ($student->active) {
                                echo "<td style='color: green'>ได้รับการยืนยันแล้ว</td>";
                            } else {
                                echo "<td style='color: red'>ยังไม่ได้รับการยืนยัน</td>";
                            }
                            echo "<td>{$student->totalScore}</td>";
                            echo "<td>{$allScore}</td>";
                            if ($student->totalScore < $baselineScore) {
                                echo "<td style='color: red'>ไม่ผ่านเกณฑ์</td>";
                            } else {
                                echo "<td style='color: green'>ผ่านเกณฑ์</td>";
                            }
                            echo "</tr>";
                        } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_3">
            <div class="box box-solid box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">กิจกรรมชุมนุม</h3>
                    <div class="box-tools pull-right">
                        <a type="button" class="btn btn-success btn-box-tool"
                           href="<?= Url::to(['activity/create', 'clubId' => $model->id]) ?>">เพิ่มกิจกรรม
                        </a>
                        <button type="button" class="btn btn-box-tool"
                                data-widget="collapse"><i
                                    class="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ชื่อ</th>
                            <th>จำนวนผู้เข้าร่วม</th>
                            <th>จำนวนผู้ไม่เข้าร่วม</th>
                            <th>ให้คะแนน</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($model->activities as $activity) { ?>
                            <tr>
                                <td><?= $activity->id ?></td>
                                <td><?= $activity->name ?></td>
                                <td><?= count($activity->studentActivities) ?></td>
                                <td><?= $studentCount - count($activity->studentActivities) ?></td>
                                <td>
                                    <a type="button" class="btn btn-sm btn-success"
                                       href="<?= Url::to(['activity/view', 'id' => $activity->id]) ?>">
                                        ให้คะแนน
                                    </a>
                                </td>
                                <td>
                                    <a href="<?= Url::to(['/activity/update', 'id' => $activity->id]) ?>"
                                       title="Update"
                                       aria-label="Update" data-pjax="0">
                                        <span class="glyphicon glyphicon-pencil"></span>
                                    </a>
                                    <a href="<?= Url::to(['/activity/delete', 'id' => $activity->id]) ?>"
                                       title="Delete"
                                       aria-label="Delete" data-pjax="0"
                                       data-confirm="Are you sure you want to delete this item?"
                                       data-method="post">
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </a>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
</div>
<!-- nav-tabs-custom -->
