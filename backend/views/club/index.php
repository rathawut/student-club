<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\ClubSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ชุมนุม';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="club-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'สร้างชุมนุม', [
            'modelClass' => 'Club',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            'name',
            [
                'attribute' => 'schoolGrades',
                'value' => function ($model) {
                    $grades = \yii\helpers\ArrayHelper::getColumn($model->schoolGrades, 'grade');
                    return implode("\n", $grades);
                },
                'format' => 'raw',
            ],
            'quota',
            'establishment_date',
            'joining_start_at',
            'joining_end_at',
            [
                'attribute' => 'teacher_advisor_id',
                'value' => 'teacherAdvisor.username',
            ],
            [
                'attribute' => 'student_leader_id',
                'value' => 'studentLeader.fullName',
            ],
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
