<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Club */

$this->title = Yii::t('backend', 'สร้าง {modelClass}', [
    'modelClass' => 'Club',
]);
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="club-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
