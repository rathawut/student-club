<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/10/2017 AD
 * Time: 2:20 PM
 */
use common\models\SchoolGrade;
use dosamigos\chartjs\ChartJs;

/** @var \common\models\AcademicYear $academicYearModel */

$activityData = [];

$firstPrimaryData = [];
$lastPrimaryData = [];
$secondaryData = [];

$absentFirstPrimaryData = [];
$absentLastPrimaryData = [];
$absentSecondaryData = [];

foreach ($academicYearModel->clubs as $club) {
    $activityData[$club->name] = $club->getActivities()->count();
}

foreach (SchoolGrade::find()->all() as $schoolGrade) {
    if (in_array($schoolGrade->id, SchoolGrade::FIRST_PRIMARY_SCHOOL_GRADE_IDS)) {
        $firstPrimaryData[$schoolGrade->grade] = 0;
        $absentFirstPrimaryData[$schoolGrade->grade] = 0;
    } else if (in_array($schoolGrade->id, SchoolGrade::LAST_PRIMARY_SCHOOL_GRADE_IDS)) {
        $lastPrimaryData[$schoolGrade->grade] = 0;
        $absentLastPrimaryData[$schoolGrade->grade] = 0;
    } else if (in_array($schoolGrade->id, SchoolGrade::SECONDARY_SCHOOL_GRADE_IDS)) {
        $secondaryData[$schoolGrade->grade] = 0;
        $absentSecondaryData[$schoolGrade->grade] = 0;
    }
}

foreach ($academicYearModel->students as $student) {
    if (intval($student->getStudentActivities()->count()) === 0) {
        if (in_array($student->schoolGrade->id, SchoolGrade::FIRST_PRIMARY_SCHOOL_GRADE_IDS)) {
            $absentFirstPrimaryData[$student->schoolGrade->grade]++;
        } else if (in_array($student->schoolGrade->id, SchoolGrade::LAST_PRIMARY_SCHOOL_GRADE_IDS)) {
            $absentLastPrimaryData[$student->schoolGrade->grade]++;
        } else if (in_array($student->schoolGrade->id, SchoolGrade::SECONDARY_SCHOOL_GRADE_IDS)) {
            $absentSecondaryData[$student->schoolGrade->grade]++;
        }
    } else {
        if (in_array($student->schoolGrade->id, SchoolGrade::FIRST_PRIMARY_SCHOOL_GRADE_IDS)) {
            $firstPrimaryData[$student->schoolGrade->grade]++;
        } else if (in_array($student->schoolGrade->id, SchoolGrade::LAST_PRIMARY_SCHOOL_GRADE_IDS)) {
            $lastPrimaryData[$student->schoolGrade->grade]++;
        } else if (in_array($student->schoolGrade->id, SchoolGrade::SECONDARY_SCHOOL_GRADE_IDS)) {
            $secondaryData[$student->schoolGrade->grade]++;
        }
    }
}
?>

<h1>กราฟกิจกรรมแต่ละชุมนุม</h1>
<div class="row">
    <div class="col-md-12">
        <div class="box box-solid box-primary">
            <div class="box-header">
                <h3 class="box-title">กราฟกิจกรรมแต่ละชุมนุม</h3>
            </div>
            <div class="box-body">
                <?= ChartJs::widget([
                    'type' => 'bar',
                    'options' => [
                        'height' => 150,
                        'width' => 500,
                    ],
                    'data' => [
                        'labels' => array_keys($activityData),
                        'datasets' => [
                            [
                                'label' => 'กราฟกิจกรรมแต่ละชุมนุม',
                                'backgroundColor' => 'rgba(179,181,198,0.2)',
                                'borderColor' => 'rgba(179,181,198,1)',
                                'pointBackgroundColor' => 'rgba(179,181,198,1)',
                                'pointBorderColor' => '#fff',
                                'pointHoverBackgroundColor' => '#fff',
                                'pointHoverBorderColor' => 'rgba(179,181,198,1)',
                                'data' => array_values($activityData),
                            ]
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>

<h1>กราฟผู้เข้าร่วมชุมนุม</h1>
<?php foreach (['ผู้เข้าร่วมชุมนุมชั้นประถมต้น' => $firstPrimaryData, 'ผู้เข้าร่วมชุมนุมชั้นประถมปลาย' => $lastPrimaryData, 'ผู้เข้าร่วมชุมนุมชั้นมัธยม' => $secondaryData] as $title => $data) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title">กราฟ<?= $title ?></h3>
                </div>
                <div class="box-body">
                    <?= ChartJs::widget([
                        'type' => 'bar',
                        'options' => [
                            'height' => 150,
                            'width' => 500,
                        ],
                        'data' => [
                            'labels' => array_keys($data),
                            'datasets' => [
                                [
                                    'label' => $title,
                                    'backgroundColor' => 'rgba(179,181,198,0.2)',
                                    'borderColor' => 'rgba(179,181,198,1)',
                                    'pointBackgroundColor' => 'rgba(179,181,198,1)',
                                    'pointBorderColor' => '#fff',
                                    'pointHoverBackgroundColor' => '#fff',
                                    'pointHoverBorderColor' => 'rgba(179,181,198,1)',
                                    'data' => array_values($data),
                                ]
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<h1>กราฟผู้ไม่เคยเข้าร่วมกิจกรรมชุมนุม</h1>
<?php foreach (['ผู้ไม่เข้าร่วมกิจกรรมชุมนุมชั้นประถมต้น' => $absentFirstPrimaryData, 'ผู้ไม่เข้าร่วมกิจกรรมชุมนุมชั้นประถมปลาย' => $absentLastPrimaryData, 'ผู้ไม่เข้าร่วมกิจกรรมชุมนุมชั้นมัธยม' => $absentSecondaryData] as $title => $data) { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="box box-solid box-primary">
                <div class="box-header">
                    <h3 class="box-title">กราฟ<?= $title ?></h3>
                </div>
                <div class="box-body">
                    <?= ChartJs::widget([
                        'type' => 'bar',
                        'options' => [
                            'height' => 150,
                            'width' => 500,
                        ],
                        'data' => [
                            'labels' => array_keys($data),
                            'datasets' => [
                                [
                                    'label' => $title,
                                    'backgroundColor' => 'rgba(179,181,198,0.2)',
                                    'borderColor' => 'rgba(179,181,198,1)',
                                    'pointBackgroundColor' => 'rgba(179,181,198,1)',
                                    'pointBorderColor' => '#fff',
                                    'pointHoverBackgroundColor' => '#fff',
                                    'pointHoverBorderColor' => 'rgba(179,181,198,1)',
                                    'data' => array_values($data),
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
