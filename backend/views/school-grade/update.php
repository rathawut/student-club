<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SchoolGrade */

$this->title = Yii::t('backend', 'แก้ไข {modelClass}: ', [
    'modelClass' => 'School Grade',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'School Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>
<div class="school-grade-update">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
