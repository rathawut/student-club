<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SchoolGrade */

$this->title = Yii::t('backend', 'สร้าง {modelClass}', [
    'modelClass' => 'School Grade',
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'School Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="school-grade-create">

    <?php echo $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
