<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 9:29 AM
 */

$academicYearModels = \common\models\AcademicYear::find()->orderBy(['year' => SORT_DESC])->all();

return [
    'options' => ['class' => 'sidebar-menu'],
    'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
    'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
    'activateParents' => true,
    'items' => [
        [
            'label' => 'ส่วนหลัก',
            'options' => ['class' => 'header']
        ],
        [
            'label' => 'รายงาน',
            'icon' => '<i class="fa fa-dashboard"></i>',
            'url' => '#',
            'items' => array_map(function ($academicYearModel) {
                /** @var $academicYearModel \common\models\AcademicYear */
                return [
                    'label' => "ปี {$academicYearModel->year}",
                    'url' => ['/dashboard/index', 'year' => $academicYearModel->year],
                ];
            }, $academicYearModels),
        ],
        [
            'label' => 'ชุมนุมในการดูแลของฉัน',
            'icon' => '<i class="fa fa-users"></i>',
            'url' => '#',
            'items' => array_map(function ($club) {
                return [
                    'label' => $club->name,
                    'url' => ['/club/view', 'id' => $club->id],
                ];
            }, Yii::$app->user->identity->clubs),
            'visible' => count(Yii::$app->user->identity->clubs) > 0,
        ],
        [
            'label' => 'ชุมนุมทั้งหมด',
            'url' => '#',
            'icon' => '<i class="fa fa-users"></i>',
            'options' => ['class' => 'treeview'],
            'items' => array_map(function ($academicYearModel) {
                /** @var $academicYearModel \common\models\AcademicYear */
                return [
                    'label' => "ปี {$academicYearModel->year}",
                    'url' => ['/club/index', 'year' => $academicYearModel->year],
                    'badge' => $academicYearModel->getClubs()->count(),
                    'badgeBgClass' => 'label-success',
                ];
            }, $academicYearModels),
            'visible' => Yii::$app->user->can('administrator')
        ],
        [
            'label' => 'นักเรียนทั้งหมด',
            'url' => '#',
            'icon' => '<i class="fa fa-child"></i>',
            'options' => ['class' => 'treeview'],
            'items' => array_map(function ($academicYearModel) {
                /** @var $academicYearModel \common\models\AcademicYear */
                return [
                    'label' => "ปี {$academicYearModel->year}",
                    'url' => ['/student/index', 'year' => $academicYearModel->year],
                    'badge' => $academicYearModel->getStudents()->count(),
                    'badgeBgClass' => 'label-success',
                ];
            }, $academicYearModels),
            'visible' => Yii::$app->user->can('administrator')
        ],
        [
            'label' => 'ปีการศึกษา',
            'icon' => '<i class="fa fa-calendar"></i>',
            'url' => ['/academic-year/index'],
            'badge' => \common\models\AcademicYear::find()->count(),
            'badgeBgClass' => 'label-success',
            'visible' => Yii::$app->user->can('administrator')
        ],
        [
            'label' => 'ระดับชั้นเรียน',
            'icon' => '<i class="fa fa-graduation-cap"></i>',
            'url' => ['/school-grade/index'],
            'badge' => \common\models\SchoolGrade::find()->count(),
            'badgeBgClass' => 'label-success',
            'visible' => Yii::$app->user->can('administrator')
        ],
        [
            'label' => 'อาจารย์ และเจ้าหน้า',
            'icon' => '<i class="fa fa-users"></i>',
            'url' => ['/user/index'],
            'visible' => Yii::$app->user->can('administrator')
        ],
    ]
];
