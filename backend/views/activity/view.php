<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Activity */

$this->title = $model->name;


$js = <<<JS
function score(id, studentId, score) {
    jQuery.get('score', {
        id: id,
        studentId: studentId,
        score: score,
    }, () => {
        location.reload();
    });
}
JS;
$this->registerJs($js, \yii\web\View::POS_BEGIN);
?>

<div class="row">
    <div class="col-md-4">
        <h2>รายละเอียดกิจกรรม</h2>
        <?php echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'reference_activity',
                'name',
                'club_id',
                'created_at',
                'updated_at',
            ],
        ]) ?>
    </div>
    <div class="col-md-8">
        <div class="box box-solid box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">สมาชิกชุมนุม</h3>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>ชื่อ</th>
                        <th>ระดับชั้น</th>
                        <th>สถานะ</th>
                        <th>คะแนน</th>
                        <th>ให้คะแนน</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($model->club->students as $index => $student) {
                        /** @var \common\models\StudentActivity $studentActivity */
                        $studentActivity = $student->getStudentActivities()->where(['activity_id' => $model->id])->one();
                        echo "<tr>";
                        echo "<td>" . ++$index . "</td>";
                        echo "<td>{$student->fullName}</td>";
                        echo "<td style='color: orange'>ชั้น{$student->schoolGrade->grade}</td>";
                        if ($student->active) {
                            echo "<td style='color: green'>ได้รับการยืนยันแล้ว</td>";
                        } else {
                            echo "<td style='color: red'>ยังไม่ได้รับการยืนยัน</td>";
                        }
                        if ($studentActivity) {
                            echo "<td>{$studentActivity->score}</td>";
                        } else {
                            echo "<td>ยังไม่ประเมิน</td>";
                        }
                        echo "<td><div class='form-group'><select class='form-control' onchange='score({$model->id}, {$student->id}, this.value)'>";
                        foreach ([10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0] as $i) {
                            echo '<option' . ($studentActivity->score == $i ? ' selected' : '') . ">$i</option>";
                        }
                        echo "</select></div></td>";
                        echo "</tr>";
                    } ?>
                    </tbody>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
