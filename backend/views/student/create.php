<?php

use common\models\Student;
use frontend\modules\user\models\SignupForm;
use kartik\builder\FormGrid;
use kartik\helpers\Html;
use kartik\form\ActiveForm;
use kartik\builder\Form;

/**
 * @var $this yii\web\View
 * @var $model Student
 */

$this->title = Yii::t('backend', 'สร้าง {modelClass}', [
    'modelClass' => 'Student',
]);
$this->params['breadcrumbs'][] = $this->title;

$emptyClubModel = new \common\models\Club();
$emptySchoolGradeModel = new \common\models\SchoolGrade();
$emptyAcademicYearModel = new \common\models\AcademicYear();
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="text-center"><?= $this->title ?></h2>
        <div>
            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $this->render('_form', [
                'form' => $form,
                'model' => $model,
            ]) ?>
            <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
