<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'นักเรียน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a(Yii::t('backend', 'สร้างนักเรียน', [
            'modelClass' => 'Student',
        ]), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php \yii\widgets\Pjax::begin() ?>
    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'identification_number',
            'title_name',
            'first_name',
            'last_name',
            // 'gender',
            // 'address:ntext',
            // 'phone_number',
            // 'nationality',
            // 'race',
            // 'religion',
            // 'school_grade_id',
            // 'academic_year_id',
            'club.name',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',
            [
                'attribute' => 'active',
                'value' => function ($model) {
                    return $model->active
                        ? "<span style='color: green'>ยืนยันแล้ว</span>"
                        : "<span style='color: red'>ยังไม่ได้รับการยืนยัน</span>";
                },
                'format' => 'html',
            ],
            [
                'class' => '\yii2mod\toggle\ToggleColumn',
                'attribute' => 'active',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
            ],
        ],
    ]); ?>
    <?php \yii\widgets\Pjax::end() ?>
</div>
