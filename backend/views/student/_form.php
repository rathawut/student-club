<?php

use common\models\SchoolGrade;
use kartik\builder\FormGrid;
use common\models\AcademicYear;
use kartik\date\DatePicker;
use yii\helpers\ArrayHelper;
use kartik\builder\Form;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/* @var $this yii\web\View */
/* @var $model common\models\Student */
/* @var $form yii\bootstrap\ActiveForm */
?>

<?= FormGrid::widget([
    'model' => $model,
    'form' => $form,
    'autoGenerateColumns' => true,
    'attributeDefaults' => [
        'type' => Form::INPUT_TEXT,
    ],
    'rows' => [
        [
            'contentBefore' => '<legend class="text-info"><small>ข้อมูลทั่วไป</small></legend>',
            'attributes' => [
                'identification_number' => [],
            ],
        ],
        [
            'attributes' => [
                'title_name' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => [
                        '' => 'กรุณาเลือก ...',
                        'เด็กชาย' => 'เด็กชาย', 'เด็กหญิง' => 'เด็กหญิง',
                        'นาย' => 'นาย', 'นางสาว' => 'นางสาว',
                    ],
                ],
                'first_name' => [],
                'last_name' => [],
                'nickname' => [],
            ],
        ],
        [
            'attributes' => [
                'gender' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => ['' => 'กรุณาเลือก ...', 'ชาย' => 'ชาย', 'หญิง' => 'หญิง'],
                ],
                'birthdate' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DatePicker::className(),
                    'options' => [
                        'pluginOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                        ],
                    ],
                ],
            ],
        ],
        [
            'attributes' => [
                'nationality' => [],
                'race' => [],
                'religion' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => ['' => 'กรุณาเลือก ...', 'อิสลาม' => 'อิสลาม', 'พุทธ' => 'พุทธ', 'อื่น ๆ' => 'อื่น ๆ'],
                ],
            ],
        ],
        [
            'attributes' => [
                'address' => [
                    'type' => Form::INPUT_TEXTAREA,
                ],
                'phone_number' => [],
            ],
        ],
        [
            'contentBefore' => '<legend class="text-info"><small>ชั้นปี / ชุมนุม</small></legend>',
            'attributes' => [
                'academic_year_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => ['' => 'กรุณาเลือก ...'] + ArrayHelper::map(AcademicYear::find()->orderBy(['year' => SORT_DESC])->asArray()->all(), 'id', 'year'),
                    'options' => [
                        'id' => 'academic_year_id-id'
                    ],
                ],
                'school_grade_id' => [
                    'type' => Form::INPUT_DROPDOWN_LIST,
                    'items' => ['' => 'กรุณาเลือก ...'] + ArrayHelper::map(SchoolGrade::find()->asArray()->all(), 'id', 'grade'),
                    'options' => [
                        'id' => 'school_grade_id-id'
                    ],
                ],
                'club_id' => [
                    'type' => Form::INPUT_WIDGET,
                    'widgetClass' => DepDrop::className(),
                    'options' => [
                        'id' => 'club_id-id',
                        'pluginOptions' => [
                            'depends' => ['academic_year_id-id', 'school_grade_id-id'],
                            'placeholder' => 'กรุณาเลือก ...',
                            'url' => Url::to([
                                'depdrop-club',
                                'selected' => $studentModel->club_id,
                            ]),
                            'initialize' => true,
                        ],
                    ],
                ],
            ],
        ],
    ],
]) ?>
