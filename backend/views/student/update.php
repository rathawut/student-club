<?php

use kartik\helpers\Html;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Student */

$this->title = Yii::t('backend', 'แก้ไข {modelClass}: ', [
    'modelClass' => 'Student',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Students'), 'url' => ['index', 'year' => $model->academicYear->year]];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');

$emptyClubModel = new \common\models\Club();
$emptySchoolGradeModel = new \common\models\SchoolGrade();
$emptyAcademicYearModel = new \common\models\AcademicYear();
?>

<div class="row">
    <div class="col-md-12">
        <h2 class="text-center"><?= $this->title ?></h2>
        <div>
            <?php $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_VERTICAL,
                'options' => ['enctype' => 'multipart/form-data'],
            ]); ?>
            <?= $this->render('_form', [
                'form' => $form,
                'model' => $model,
            ]) ?>
            <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
            <?php ActiveForm::end() ?>
        </div>
    </div>
</div>
