<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/10/2017 AD
 * Time: 2:17 PM
 */

namespace backend\controllers;


use common\models\AcademicYear;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DashboardController extends Controller
{
    public function actionIndex($year = null)
    {
        $academicYearModel = $this->findAcademicYearModel($year);
        return $this->render('index', [
            'academicYearModel' => $academicYearModel,
        ]);
    }

    public function findAcademicYearModel($year)
    {
        if ($year) {
            $academicYearModel = AcademicYear::find()->year($year)->one();
        } else {
            $academicYearModel = AcademicYear::find()->orderBy(['id' => SORT_DESC])->one();
        }
        if ($academicYearModel === null) {
            throw new NotFoundHttpException('ปีการศึกษาที่เลือกไม่มี');
        }
        return $academicYearModel;
    }
}
