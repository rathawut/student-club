<?php

namespace backend\controllers;

use common\models\AcademicYear;
use common\models\SchoolGrade;
use common\models\User;
use frontend\modules\user\models\SignupForm;
use Yii;
use common\models\Student;
use backend\models\StudentSearch;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'toggle' => [
                'class' => \yii2mod\toggle\actions\ToggleAction::class,
                'modelClass' => Student::class,
                // if you want to use flash messages
                'setFlash' => true,
                'flashSuccess' => 'สำเร็จ',
                'flashError' => 'พบปัญหา',
                'redirect' => 'index',
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @param $year
     * @return mixed
     */
    public function actionIndex($year)
    {
        $academicYearModel = $this->findAcademicYearModel($year);

        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $academicYearModel->getStudents());

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Student();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', [
                'body' => "{$model->fullName} ได้สมัครเข้า {$model->club->name} แล้ว",
                'options' => ['class' => 'alert-success'],
            ]);
            return $this->redirect(['index', 'year' => $model->academicYear->year]);
        }
        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('alert', [
                'body' => "ได้อัพเดทข้อมูลของ {$model->fullName} แล้ว",
                'options' => ['class' => 'alert-success'],
            ]);
            return $this->redirect(['index', 'year' => $model->academicYear->year]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDepdropClub($selected = '')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if (is_array($parents) && count($parents) === 2 && $parents[0] !== '') {
                $academicYearModel = AcademicYear::findOne($parents[0]);
                $schoolGradeModel = SchoolGrade::findOne($parents[1]);
                $output = $academicYearModel
                    ->getClubs()
                    ->select(['id', 'name'])
                    ->where(['id' => $schoolGradeModel->getClubs()->select('id')])
                    ->asArray()
                    ->all();
                return [
                    'output' => $output,
                    'selected' => $selected,
                ];
            }
        }
        return ['output' => '', 'selected' => $selected];
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $year = $model->academicYear->year;
        $model->delete();
        return $this->redirect(['index', 'year' => $year]);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function findAcademicYearModel($year)
    {
        if (($academicYearModel = AcademicYear::find()->year($year)->one()) === null) {
            throw new NotFoundHttpException('ปีการศึกษาที่เลือกไม่มี');
        }
        return $academicYearModel;
    }
}
