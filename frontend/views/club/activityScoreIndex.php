<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 3:17 PM
 */
use common\models\Student;
use kartik\form\ActiveForm;
use kartik\helpers\Html;

/** @var Student $studentModel */
?>

<?php $form = ActiveForm::begin([
    'id' => 'activity-score',
    'options' => ['class' => 'form-horizontal'],
]) ?>
<div class="row">
    <div class="col-md-4 col-md-offset-4 text-center">
        <?= $form->field($studentModel, 'identification_number') ?>
        <div class="form-group">
            <?= Html::submitButton('Check', ['class' => 'btn btn-primary']) ?>
        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
