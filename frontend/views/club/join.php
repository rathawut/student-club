<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 10:20 AM
 */

use common\models\SchoolGrade;
use common\models\Student;
use kartik\builder\FormGrid;
use kartik\date\DatePicker;
use kartik\helpers\Html;
use common\models\AcademicYear;
use yii\helpers\ArrayHelper;
use kartik\form\ActiveForm;
use kartik\builder\Form;
use yii\helpers\Url;
use kartik\depdrop\DepDrop;

/**
 * @var $studentModel Student
 * @var $recentStudentModels Student[]
 */
$this->title = 'สมัครเข้าชุมนุม (สำหรับนักเรียน)';

$emptyClubModel = new \common\models\Club();
$emptySchoolGradeModel = new \common\models\SchoolGrade();
$emptyAcademicYearModel = new \common\models\AcademicYear();
?>

<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h2 class="text-center"><?= $this->title ?></h2>
            <div>
                <?php $form = ActiveForm::begin([
                    'type' => ActiveForm::TYPE_VERTICAL,
                    'options' => ['enctype' => 'multipart/form-data'],
                ]); ?>
                <?= FormGrid::widget([
                    'model' => $studentModel,
                    'form' => $form,
                    'autoGenerateColumns' => true,
                    'attributeDefaults' => [
                        'type' => Form::INPUT_TEXT,
                    ],
                    'rows' => [
                        [
                            'contentBefore' => '<legend class="text-info"><small>ข้อมูลทั่วไป</small></legend>',
                            'attributes' => [
                                'identification_number' => [],
                            ],
                        ],
                        [
                            'attributes' => [
                                'title_name' => [
                                    'type' => Form::INPUT_DROPDOWN_LIST,
                                    'items' => [
                                        '' => 'กรุณาเลือก ...',
                                        'เด็กชาย' => 'เด็กชาย', 'เด็กหญิง' => 'เด็กหญิง',
                                        'นาย' => 'นาย', 'นางสาว' => 'นางสาว',
                                    ],
                                ],
                                'first_name' => [],
                                'last_name' => [],
                                'nickname' => [],
                            ],
                        ],
                        [
                            'attributes' => [
                                'gender' => [
                                    'type' => Form::INPUT_DROPDOWN_LIST,
                                    'items' => ['' => 'กรุณาเลือก ...', 'ชาย' => 'ชาย', 'หญิง' => 'หญิง'],
                                ],
                                'birthdate' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => DatePicker::className(),
                                    'options' => [
                                        'pluginOptions' => [
                                            'format' => 'yyyy-mm-dd',
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            'attributes' => [
                                'nationality' => [],
                                'race' => [],
                                'religion' => [
                                    'type' => Form::INPUT_DROPDOWN_LIST,
                                    'items' => ['' => 'กรุณาเลือก ...', 'อิสลาม' => 'อิสลาม', 'พุทธ' => 'พุทธ', 'อื่น ๆ' => 'อื่น ๆ'],
                                ],
                            ],
                        ],
                        [
                            'attributes' => [
                                'address' => [
                                    'type' => Form::INPUT_TEXTAREA,
                                ],
                                'phone_number' => [],
                            ],
                        ],
                        [
                            'contentBefore' => '<legend class="text-info"><small>ชั้นปี / ชุมนุม</small></legend>',
                            'attributes' => [
                                'academic_year_id' => [
                                    'type' => Form::INPUT_DROPDOWN_LIST,
                                    'items' => ['' => 'กรุณาเลือก ...'] + ArrayHelper::map(AcademicYear::find()->active()->orderBy(['year' => SORT_DESC])->asArray()->all(), 'id', 'year'),
                                    'options' => [
                                        'id' => 'academic_year_id-id'
                                    ],
                                ],
                                'school_grade_id' => [
                                    'type' => Form::INPUT_DROPDOWN_LIST,
                                    'items' => ['' => 'กรุณาเลือก ...'] + ArrayHelper::map(SchoolGrade::find()->asArray()->all(), 'id', 'grade'),
                                    'options' => [
                                        'id' => 'school_grade_id-id'
                                    ],
                                ],
                                'club_id' => [
                                    'type' => Form::INPUT_WIDGET,
                                    'widgetClass' => DepDrop::className(),
                                    'options' => [
                                        'id' => 'club_id-id',
                                        'pluginOptions' => [
                                            'depends' => ['academic_year_id-id', 'school_grade_id-id'],
                                            'placeholder' => 'กรุณาเลือก ...',
                                            'url' => Url::to([
                                                'depdrop-club',
                                                'selected' => $studentModel->club_id,
                                            ]),
                                            'initialize' => true,
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ]) ?>
                <?= Html::submitButton('บันทึก', ['class' => 'btn btn-success']) ?>
                <?php ActiveForm::end() ?>
            </div>
        </div>
        <div class="col-md-6">
            <h2 class="text-center">ผู้เข้าร่วมชุมนุมล่าสุด</h2>
            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th><?= $studentModel->getAttributeLabel('fullName') ?></th>
                    <th><?= $emptyClubModel->getAttributeLabel('name') ?></th>
                    <th><?= $emptyAcademicYearModel->getAttributeLabel('year') ?></th>
                    <th><?= $emptySchoolGradeModel->getAttributeLabel('grade') ?></th>
                    <th><?= $emptySchoolGradeModel->getAttributeLabel('active') ?></th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($recentStudentModels as $recentStudentModel) { ?>
                    <tr>
                        <td><?= $recentStudentModel->id ?></td>
                        <td><?= $recentStudentModel->fullName ?></td>
                        <td><?= $recentStudentModel->club->name ?></td>
                        <td><?= $recentStudentModel->academicYear->year ?></td>
                        <td><?= $recentStudentModel->schoolGrade->grade ?></td>
                        <td><?= $recentStudentModel->active
                                ? "<span style='color: green'>ได้รับการยืนยัน</span>"
                                : "<span style='color: red'>รอการยืนยัน</span>" ?>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
