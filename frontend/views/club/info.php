<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 11:49 AM
 */
use common\models\AcademicYear;

/** @var $academicYearModel AcademicYear */
$this->title = "ข้อมูลชุมนุม ปี {$academicYearModel->year}";

$formatter = Yii::$app->formatter;
?>
<div class="container">
    <h2><?= $this->title ?></h2>
    <p>เปิดให้สมัครจำนวนทั้งสิ้น <?= count($academicYearModel->clubs) ?> ชุมนุม</p>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>#</th>
            <th>ชื่อชุมนุม</th>
            <th>ระดับชั้นที่สามารถเข้าร่วม</th>
            <th>เปิดรับ</th>
            <th>ปิดรับ</th>
            <th>รับทั้งหมด</th>
            <th>จำนวนผู้สมัครเข้าร่วมปัจจุบัน</th>
            <th>สถานะ</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($academicYearModel->clubs as $model) { ?>
            <tr>
                <td><?= $model->id ?></td>
                <td><?= $model->name ?></td>
                <td>
                    <?php $grades = array_map(function ($schoolGrade) {
                        return $schoolGrade->grade;
                    }, $model->schoolGrades);
                    $t =  implode(', ', $grades);
                    $t = str_replace('ประถมศึกษา 1, ประถมศึกษา 2, ประถมศึกษา 3', 'ประถมศึกษา 1 - 3', $t);
                    $t = str_replace('ประถมศึกษา 4, ประถมศึกษา 5, ประถมศึกษา 6', 'ประถมศึกษา 4 - 6', $t);
                    $t = str_replace('มัธยมศึกษา 1, มัธยมศึกษา 2, มัธยมศึกษา 3', 'มัธยมศึกษา 1 - 3', $t);
                    echo $t;
                    ?>
                </td>
                <td><?= $formatter->asDatetime($model->joining_start_at) ?></td>
                <td><?= $formatter->asDatetime($model->joining_end_at) ?></td>
                <td><?= $model->quota ?></td>
                <td><?= count($model->students) ?></td>
                <td><?= $model->isFull
                        ? "<span style='color: red'>ปิดรับสมัคร</span>"
                        : "<span style='color: green'>เปิดรับสมัคร</span>" ?>
                </td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
</div>
