<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 3:17 PM
 */

use common\models\Student;

/** @var Student $studentModel */

$emptyActivityModel = new \common\models\Activity();
$emptyStudentActivityModel = new \common\models\StudentActivity();
?>

<div class="container">
    <h2><?= $studentModel->club->name ?></h2>
    <p>ปีการศึกษา <?= $studentModel->academicYear->year ?></p>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">ข้อมูลนักเรียน</h3>
                </div>
                <div class="panel-body">
                    <dl class="dl-horizontal">
                        <dt><?= $studentModel->getAttributeLabel('fullName') ?></dt>
                        <dd><?= $studentModel->fullName ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('identification_number') ?></dt>
                        <dd><?= $studentModel->identification_number ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('active') ?></dt>
                        <dd><?= $studentModel->active
                                ? "<span style='color: green'>ได้รับการยืนยัน</span>"
                                : "<span style='color: red'>รอการยืนยัน</span>" ?>
                        </dd>
                        <dt><?= $studentModel->schoolGrade->getAttributeLabel('grade') ?></dt>
                        <dd><?= $studentModel->schoolGrade->grade ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('gender') ?></dt>
                        <dd><?= $studentModel->gender ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('address') ?></dt>
                        <dd><?= $studentModel->address ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('phone_number') ?></dt>
                        <dd><?= $studentModel->phone_number ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('nationality') ?></dt>
                        <dd><?= $studentModel->nationality ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('race') ?></dt>
                        <dd><?= $studentModel->race ?></dd>
                        <dt><?= $studentModel->getAttributeLabel('religion') ?></dt>
                        <dd><?= $studentModel->religion ?></dd>
                    </dl>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">ข้อมูลการประเมินผลกิจกรรม</h3>
                </div>
                <div class="panel-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th><?= $emptyActivityModel->getAttributeLabel('name') ?></th>
                            <th><?= $emptyStudentActivityModel->getAttributeLabel('score') ?></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if (count($studentModel->studentActivities) === 0) { ?>
                            <tr>
                                <td class="text-center" colspan="3">ไม่มีข้อมูล</td>
                            </tr>
                        <?php } ?>
                        <?php $allScore = 0;
                        $totalScore = 0;
                        foreach ($studentModel->studentActivities as $index => $studentActivity) {
                            $allScore += 10;
                            $totalScore += $studentActivity->score; ?>
                            <tr>
                                <td><?= ++$index ?></td>
                                <td><?= $studentActivity->activity->name ?></td>
                                <td><?= $studentActivity->score ?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <?php if ($totalScore == 0) {
                        echo "<span>ยังไม่มีคะแนน</span>";
                    } else if ($totalScore < ($allScore / 2)) {
                        echo "<span style='color: red'>ไม่ผ่านเกณฑ์</span>";
                    } else {
                        echo "<span style='color: green'>ผ่านเกณฑ์</span>";
                    } ?>
                    <?= "คะแนนรวม $totalScore / $allScore" ?>
                </div>
            </div>
        </div>
    </div>
</div>
