<?php
/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = Yii::$app->name;
?>
<div class="site-index">
    <img src="<?= Url::to('@web/img/header.jpg') ?>" style="width: 100%;">
    <div class="jumbotron">
        <h1>ปรัชญา</h1>
        <p class="lead">เรียนรู้ คู่คุณธรรม นำเผยแพร่</p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-6">
                <div class="panel panel-info panel-default">
                    <div class="panel-heading">พันธกิจ</div>
                    <div class="panel-body">
                        <ul>
                            <li>เป็นโรงเรียนสมบูรณ์แบบในทุกๆด้าน(ศาสนา สามัญ)</li>
                            <li>นำหลักของศาสนาอิสลามมาใช้ในการดำเนินชีวิต(คุณธรรม จริยธรรม)</li>
                            <li>
                                สนองความต้องการของชุมชน(ผู้เกี่ยวข้องพึงพอใจในการดำเนินของโรงเรียน)
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="panel panel-info panel-default">
                    <div class="panel-heading">หลักการจัดกิจกรรมพัฒนาผู้เรียน</div>
                    <div class="panel-body">
                        <ol>
                            <li>จัดให้ผู้เรียนได้พัฒนาความสามารถของตนเองตามศักยภาพ</li>
                            <li>จัดให้ผู้เรียนได้เพิ่มเติมจากกิจกรรมที่ได้เรียนรู้ตามกลุ่มสาระทั้ง 8
                                กลุ่ม
                            </li>
                            <li>
                                จัดให้ผู้เรียนได้เข้าร่วมและปฏิบัติกิจกรรมที่เหมาะสมร่วมกับผู้อื่นอย่างมีความสุขกับกิจกรรมที่เลือกด้วยตามความถนัด
                                ความสนใจอย่างแท้จริง
                            </li>
                        </ol>
                    </div>
                </div>
                <h2></h2>
            </div>
        </div>

    </div>
</div>
