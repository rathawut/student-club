<?php

use common\models\AcademicYear;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;

/* @var $this \yii\web\View */
/* @var $content string */

$this->beginContent('@frontend/views/layouts/_clear.php')
?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]); ?>
    <?php echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'หน้าแรก', 'url' => '/site/index'],
            [
                'label' => 'ข้อมูลชุมนุม',
                'url' => '/club/info',
                'items' => array_map(function ($academicYearModel) {
                    return [
                        'label' => "ปี {$academicYearModel['year']}",
                        'url' => ['/club/info', 'year' => $academicYearModel['year']],
                    ];
                }, AcademicYear::find()->orderBy(['year' => SORT_DESC])->asArray()->all()),
            ],
            ['label' => 'สมัครเข้าชุมนุม (สำหรับนักเรียน)', 'url' => ['/club/join']],
            [
                'label' => 'ตรวจสอบผลประเมินกิจกรรม',
                'items' => array_map(function ($academicYearModel) {
                    return [
                        'label' => "ปี {$academicYearModel['year']}",
                        'url' => ['/club/activity-score', 'year' => $academicYearModel['year']],
                    ];
                }, AcademicYear::find()->orderBy(['year' => SORT_DESC])->asArray()->all()),
            ],
            // ['label' => '', 'url' => ['/site/contact']],
            // ['label' => '', 'url' => ['/user/sign-in/signup'], 'visible' => Yii::$app->user->isGuest],
            ['label' => 'ล็อกอิน', 'url' => ['/user/sign-in/login'], 'visible' => Yii::$app->user->isGuest],
            [
                'label' => Yii::$app->user->isGuest ? '' : Yii::$app->user->identity->getPublicIdentity(),
                'visible' => !Yii::$app->user->isGuest,
                'items' => [
                    [
                        'label' => 'ตั้งค่า',
                        'url' => ['/user/default/index']
                    ],
                    [
                        'label' => 'ระบบหลังบ้าน',
                        'url' => Yii::getAlias('@backendUrl'),
                        'visible' => Yii::$app->user->can(\common\models\User::ROLE_TEACHER)
                    ],
                    [
                        'label' => Yii::t('frontend', 'Logout'),
                        'url' => ['/user/sign-in/logout'],
                        'linkOptions' => ['data-method' => 'post']
                    ]
                ]
            ],
//            [
//                'label' => Yii::t('frontend', 'Language'),
//                'items' => array_map(function ($code) {
//                    return [
//                        'label' => Yii::$app->params['availableLocales'][$code],
//                        'url' => ['/site/set-locale', 'locale' => $code],
//                        'active' => Yii::$app->language === $code
//                    ];
//                }, array_keys(Yii::$app->params['availableLocales']))
//            ]
        ]
    ]); ?>
    <?php NavBar::end(); ?>

    <?php echo $content ?>

</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?php echo date('Y') ?></p>
        <p class="pull-right"><?php echo Yii::powered() ?></p>
    </div>
</footer>
<?php $this->endContent() ?>
