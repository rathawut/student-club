<?php
/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 9/9/2017 AD
 * Time: 10:15 AM
 */

namespace frontend\controllers;

use common\base\MultiModel;
use common\models\AcademicYear;
use common\models\Club;
use common\models\SchoolGrade;
use common\models\Student;
use common\models\User;
use frontend\modules\user\models\SignupForm;
use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class ClubController extends Controller
{
    public function actionInfo($year)
    {
        $academicYearModel = $this->findAcademicYearModel($year);
        return $this->render('info', [
            'academicYearModel' => $academicYearModel,
        ]);
    }

    public function actionJoin()
    {
        $studentModel = new Student();
        $recentStudentModels = Student::find()
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(30)
            ->all();
        if ($studentModel->load(Yii::$app->request->post()) && $studentModel->save()) {

            Yii::$app->session->setFlash('alert', [
                'body' => "{$studentModel->fullName} ได้สมัครเข้า {$studentModel->club->name} แล้ว",
                'options' => ['class' => 'alert-success'],
            ]);
            return $this->redirect(['join']);
        }
        return $this->render('join', [
            'studentModel' => $studentModel,
            'recentStudentModels' => $recentStudentModels,
        ]);
    }

    public function actionDepdropClub($selected = '')
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            if (is_array($parents) && count($parents) === 2 && $parents[0] !== '') {
                $academicYearModel = AcademicYear::findOne($parents[0]);
                $schoolGradeModel = SchoolGrade::findOne($parents[1]);
                $output = $academicYearModel
                    ->getClubs()
                    ->select(['id', 'name'])
                    ->where(['id' => $schoolGradeModel->getClubs()->select('id')])
                    ->asArray()
                    ->all();
                return [
                    'output' => $output,
                    'selected' => $selected,
                ];
            }
        }
        return ['output' => '', 'selected' => $selected];
    }

    public function actionActivityScore($year)
    {
        $academicYearModel = $this->findAcademicYearModel($year);
        $studentModel = new Student(['scenario' => Student::SCENARIO_CHECK_BY_ID_NUMBER]);
        $studentModel->academic_year_id = $academicYearModel->id;
        if ($studentModel->load(Yii::$app->request->post()) && $studentModel->validate()) {
            $checkedStudentModel = Student::findOne([
                'identification_number' => $studentModel->identification_number,
                'academic_year_id' => $studentModel->academic_year_id,
            ]);
            if ($checkedStudentModel) {
                return $this->render('activityScore', [
                    'studentModel' => $checkedStudentModel,
                ]);
            }
            Yii::$app->session->setFlash('alert', [
                'body' => "ไม่พบ {$studentModel->identification_number} ในระบบ",
                'options' => ['class' => 'alert-danger'],
            ]);
        }
        return $this->render('activityScoreIndex', [
            'studentModel' => $studentModel,
        ]);
    }

    public function findAcademicYearModel($year)
    {
        if (($academicYearModel = AcademicYear::find()->year($year)->one()) === null) {
            throw new NotFoundHttpException('ปีการศึกษาที่เลือกไม่มี');
        }
        return $academicYearModel;
    }
}
