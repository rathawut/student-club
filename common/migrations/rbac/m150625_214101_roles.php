<?php

use common\rbac\Migration;
use common\models\User;

class m150625_214101_roles extends Migration
{
    public function up()
    {
        $this->auth->removeAll();

        $user = $this->auth->createRole(User::ROLE_USER);
        $this->auth->add($user);

        $teacher = $this->auth->createRole(User::ROLE_TEACHER);
        $this->auth->add($teacher);
        $this->auth->addChild($teacher, $user);

        $admin = $this->auth->createRole(User::ROLE_ADMINISTRATOR);
        $this->auth->add($admin);
        $this->auth->addChild($admin, $teacher);

        $this->auth->assign($admin, 1);
        $this->auth->assign($teacher, 2);
        $this->auth->assign($user, 3);
    }

    public function down()
    {
        $this->auth->remove($this->auth->getRole(User::ROLE_ADMINISTRATOR));
        $this->auth->remove($this->auth->getRole(User::ROLE_TEACHER));
        $this->auth->remove($this->auth->getRole(User::ROLE_USER));
    }
}
