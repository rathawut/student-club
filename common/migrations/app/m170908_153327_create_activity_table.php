<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `activity`.
 */
class m170908_153327_create_activity_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTableWithBlameAndTimestamp('{{%activity}}', [
            'id' => $this->primaryKey(),
            'reference_activity' => [
                'type' => $this->integer(),
                'fk' => '{{%activity}}',
            ],
            'name' => $this->string()->notNull(),
            'club_id' => ['fk' => '{{%club}}'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTableWithBlameAndTimestamp('{{%activity}}');
    }
}
