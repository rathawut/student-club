<?php

use yii\db\Migration;

class m170909_021611_seed extends Migration
{
    public function safeUp()
    {
        try {
            $this->insert('{{%academic_year}}', [
                'year' => '2560',
                'created_by' => 1,
                'updated_by' => 1,
            ]);
            $this->batchInsert('{{%school_grade}}', ['grade', 'created_by', 'updated_by'], [
                ['ประถมศึกษา 1', 1, 1],
                ['ประถมศึกษา 2', 1, 1],
                ['ประถมศึกษา 3', 1, 1],
                ['ประถมศึกษา 4', 1, 1],
                ['ประถมศึกษา 5', 1, 1],
                ['ประถมศึกษา 6', 1, 1],
                ['มัธยมศึกษา 1', 1, 1],
                ['มัธยมศึกษา 2', 1, 1],
                ['มัธยมศึกษา 3', 1, 1],
            ]);
            $this->batchInsert('{{%club}}', ['name', 'academic_year_id', 'quota', 'created_by', 'updated_by'], [
                ['ชุมนุมรักการอ่าน', 1, 50, 1, 1],
                ['ชุมนุมภาษาไทย', 1, 50, 1, 1],
                ['ชุมนุมภาษาอังกฤษ', 1, 50, 1, 1],
                ['ชุมนุมภาษาอาหรับ', 1, 50, 1, 1],
                ['ชุมนุม อ.ย น้อย', 1, 50, 1, 1],
                ['ชุมนุมศิลปะ', 1, 50, 1, 1],
                ['ชุมนุมนักประดิษฐ์', 1, 50, 1, 1],

                ['ชุมนุมคอมพิวเตอร์', 1, 50, 1, 1],
                ['ชุมนุมนักกอรีย์', 1, 50, 1, 1],
                ['ชุมนุมอาสาพัฒนา', 1, 50, 1, 1],
                ['ชุมนุมรักสิ่งแวดล้อม', 1, 50, 1, 1],
                ['ชุมนุมคนรักการอ่าน', 1, 50, 1, 1],
            ]);
            $clubSchoolGradeData = [];
            // ประถม
            foreach ([1, 2, 3, 4, 5, 6, 7] as $clubId) {
                foreach ([1, 2, 3, 4, 5, 6] as $schoolGradeId) {
                    $clubSchoolGradeData[] = [$clubId, $schoolGradeId];
                }
            }
            // มัธยม
            foreach ([8, 9, 10, 11, 12] as $clubId) {
                foreach ([7, 8, 9] as $schoolGradeId) {
                    $clubSchoolGradeData[] = [$clubId, $schoolGradeId];
                }
            }
            $this->batchInsert('{{club_school_grade}}', ['club_id', 'school_grade_id'], $clubSchoolGradeData);
        } catch (\yii\db\IntegrityException $exception) {
            echo "Ignore seeding\n";
        }
    }

    public function safeDown()
    {
    }
}
