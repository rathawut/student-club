<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `academic_year`.
 */
class m170908_144130_create_academic_year_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTableWithBlameAndTimestamp('{{%academic_year}}', [
            'id' => $this->primaryKey(),
            'year' => $this->string()->notNull()->unique(),
            'active' => $this->boolean(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTableWithBlameAndTimestamp('{{%academic_year}}');
    }
}
