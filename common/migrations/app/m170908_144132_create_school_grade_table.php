<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `school_grade`.
 */
class m170908_144132_create_school_grade_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTableWithBlameAndTimestamp('{{%school_grade}}', [
            'id' => $this->primaryKey(),
            'grade' => $this->string()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTableWithBlameAndTimestamp('{{%school_grade}}');
    }
}
