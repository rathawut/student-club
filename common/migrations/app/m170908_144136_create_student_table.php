<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `student`.
 */
class m170908_144136_create_student_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTablePro('{{%student}}', [
            'id' => $this->primaryKey(),
            'identification_number' => $this->string(),
            'title_name' => $this->string(),
            'first_name' => $this->string(),
            'last_name' => $this->string(),
            'nickname' => $this->string(),
            'birthdate' => $this->date(),
            'gender' => $this->string(),
            'address' => $this->text(),
            'phone_number' => $this->string(),
            'nationality' => $this->string(),
            'race' => $this->string(),
            'religion' => $this->string(),
            'active' => $this->boolean(),
            'school_grade_id' => ['fk' => '{{%school_grade}}'],
            'academic_year_id' => ['fk' => '{{%academic_year}}'],
            'club_id' => $this->integer()->notNull(),
            'created_at' => 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%student}}');
    }
}
