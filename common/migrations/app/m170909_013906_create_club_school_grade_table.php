<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `club_school_grade`.
 */
class m170909_013906_create_club_school_grade_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createJunctionTable(
            '{{%club_school_grade}}',
            '{{%club}}',
            '{{%school_grade}}',
            'club_id',
            'school_grade_id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropJunctionTable(
            '{{%club_school_grade}}',
            'club_id',
            'school_grade_id'
        );
    }
}
