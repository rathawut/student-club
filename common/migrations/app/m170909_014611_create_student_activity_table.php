<?php

use common\traits\MigrationTrait;
use yii\db\Migration;

/**
 * Handles the creation of table `student_activity`.
 */
class m170909_014611_create_student_activity_table extends Migration
{
    use MigrationTrait;

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTableWithBlameAndTimestamp('{{%student_activity}}', [
            'id' => $this->primaryKey(),
            'student_id' => ['fk' => '{{%student}}'],
            'activity_id' => ['fk' => '{{%activity}}'],
            'score' => $this->float(2),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTableWithBlameAndTimestamp('{{%student_activity}}');
    }
}
