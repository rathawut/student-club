<?php

use yii\db\Migration;

class m000001_000001_set_collation_db extends Migration
{
    public function safeUp()
    {
        // TODO: Don't fix database name
        $this->execute('ALTER DATABASE `yii2-starter-kit` CHARACTER SET utf8 COLLATE utf8_unicode_ci');
    }

    public function safeDown()
    {
    }
}
