<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[SchoolGrade]].
 *
 * @see SchoolGrade
 */
class SchoolGradeQuery extends \yii\db\ActiveQuery
{
    public function primarySchool()
    {
        return $this->andWhere(['id' => SchoolGrade::PRIMARY_SCHOOL_GRADE_IDS]);
    }

    public function secondarySchool()
    {
        return $this->andWhere(['id' => SchoolGrade::SECONDARY_SCHOOL_GRADE_IDS]);
    }

    /**
     * @inheritdoc
     * @return SchoolGrade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return SchoolGrade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
