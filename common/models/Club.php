<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%club}}".
 *
 * @property integer $id
 * @property string $name
 * @property integer $academic_year_id
 * @property integer $quota
 * @property string $establishment_date
 * @property string $joining_start_at
 * @property string $joining_end_at
 * @property integer $teacher_advisor_id
 * @property integer $student_leader_id
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Activity[] $activities
 * @property AcademicYear $academicYear
 * @property User $createdBy
 * @property Student $studentLeader
 * @property User $teacherAdvisor
 * @property User $updatedBy
 * @property ClubSchoolGrade[] $clubSchoolGrades
 * @property SchoolGrade[] $schoolGrades
 * @property Student[] $students
 *
 * @property bool $isFull
 */
class Club extends \yii\db\ActiveRecord
{
    public $school_grade_ids;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%club}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'academic_year_id'], 'required'],
            [['academic_year_id', 'quota', 'teacher_advisor_id', 'student_leader_id', 'created_by', 'updated_by'], 'integer'],
            [['establishment_date', 'joining_start_at', 'joining_end_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['academic_year_id'], 'exist', 'skipOnError' => true, 'targetClass' => AcademicYear::className(), 'targetAttribute' => ['academic_year_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['student_leader_id'], 'exist', 'skipOnError' => true, 'targetClass' => Student::className(), 'targetAttribute' => ['student_leader_id' => 'id']],
            [['teacher_advisor_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['teacher_advisor_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['school_grade_ids', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัส',
            'name' => 'ชื่อชุมนุม',
            'academic_year_id' => 'ปีการศึกษา',
            'quota' => 'จำนวนที่รับ',
            'joining_start_at' => 'เวลาเริ่มรับ',
            'joining_end_at' => 'เวลาปิดรับ',
            'teacher_advisor_id' => 'อาจารย์ที่ปรึกษา',
            'student_leader_id' => 'ประธานนักเรียน',
            'schoolGrades' => 'ระดับชั้น',
            'school_grade_ids' => 'ระดับชั้น',
            'establishment_date' => 'วันก่อตั้ง',
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors()
    {
        return [BlameableBehavior::className()];
    }

    public function afterFind()
    {
        $this->school_grade_ids = ArrayHelper::getColumn($this->schoolGrades, 'id');
        parent::afterFind();
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        foreach ($this->clubSchoolGrades as $clubSchoolGrade) {
            $clubSchoolGrade->delete();
        }
        if (!empty($this->school_grade_ids)) {
            foreach ($this->school_grade_ids as $school_grade_id) {
                $clubSchoolGrade = new ClubSchoolGrade();
                $clubSchoolGrade->club_id = $this->id;
                $clubSchoolGrade->school_grade_id = $school_grade_id;
                $clubSchoolGrade->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasMany(Activity::className(), ['club_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(AcademicYear::className(), ['id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentLeader()
    {
        return $this->hasOne(Student::className(), ['id' => 'student_leader_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacherAdvisor()
    {
        return $this->hasOne(User::className(), ['id' => 'teacher_advisor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClubSchoolGrades()
    {
        return $this->hasMany(ClubSchoolGrade::className(), ['club_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolGrades()
    {
        return $this->hasMany(SchoolGrade::className(), ['id' => 'school_grade_id'])->viaTable('{{%club_school_grade}}', ['club_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['club_id' => 'id']);
    }

    public function getIsFull()
    {
        return intval($this->getStudents()->count()) >= $this->quota;
    }

    /**
     * @inheritdoc
     * @return ClubQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClubQuery(get_called_class());
    }
}
