<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%club_school_grade}}".
 *
 * @property integer $club_id
 * @property integer $school_grade_id
 *
 * @property Club $club
 * @property SchoolGrade $schoolGrade
 */
class ClubSchoolGrade extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%club_school_grade}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['club_id', 'school_grade_id'], 'required'],
            [['club_id', 'school_grade_id'], 'integer'],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Club::className(), 'targetAttribute' => ['club_id' => 'id']],
            [['school_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolGrade::className(), 'targetAttribute' => ['school_grade_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'club_id' => Yii::t('app', 'Club ID'),
            'school_grade_id' => Yii::t('app', 'School Grade ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Club::className(), ['id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolGrade()
    {
        return $this->hasOne(SchoolGrade::className(), ['id' => 'school_grade_id']);
    }

    /**
     * @inheritdoc
     * @return ClubSchoolGradeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ClubSchoolGradeQuery(get_called_class());
    }
}
