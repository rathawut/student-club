<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[AcademicYear]].
 *
 * @see AcademicYear
 */
class AcademicYearQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    public function year($year)
    {
        return $this->andWhere(['year' => $year]);
    }

    /**
     * @inheritdoc
     * @return AcademicYear[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return AcademicYear|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
