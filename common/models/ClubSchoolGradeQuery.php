<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[ClubSchoolGrade]].
 *
 * @see ClubSchoolGrade
 */
class ClubSchoolGradeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return ClubSchoolGrade[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return ClubSchoolGrade|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
