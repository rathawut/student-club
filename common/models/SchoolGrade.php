<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;

/**
 * This is the model class for table "{{%school_grade}}".
 *
 * @property integer $id
 * @property string $grade
 * @property string $created_at
 * @property string $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property ClubSchoolGrade[] $clubSchoolGrades
 * @property Club[] $clubs
 * @property User $createdBy
 * @property User $updatedBy
 * @property Student[] $students
 */
class SchoolGrade extends \yii\db\ActiveRecord
{
    CONST FIRST_PRIMARY_SCHOOL_GRADE_IDS = [1, 2, 3];
    CONST LAST_PRIMARY_SCHOOL_GRADE_IDS = [4, 5, 6];
    CONST PRIMARY_SCHOOL_GRADE_IDS = [1, 2, 3, 4, 5, 6];
    CONST SECONDARY_SCHOOL_GRADE_IDS = [7, 8, 9];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%school_grade}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['created_by', 'updated_by'], 'integer'],
            [['grade'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'grade' => 'ระดับชั้น',
            'active' => 'การยืนยัน',
            'created_at' => 'สร้างเมื่อ',
            'updated_at' => 'แก้ไขเมื่อ',
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
        ];
    }

    public function behaviors()
    {
        return [BlameableBehavior::className()];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClubSchoolGrades()
    {
        return $this->hasMany(ClubSchoolGrade::className(), ['school_grade_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClubs()
    {
        return $this->hasMany(Club::className(), ['id' => 'club_id'])->viaTable('{{%club_school_grade}}', ['school_grade_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['school_grade_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return SchoolGradeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SchoolGradeQuery(get_called_class());
    }
}
