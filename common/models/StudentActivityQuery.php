<?php

namespace common\models;

/**
 * This is the ActiveQuery class for [[StudentActivity]].
 *
 * @see StudentActivity
 */
class StudentActivityQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return StudentActivity[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return StudentActivity|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
