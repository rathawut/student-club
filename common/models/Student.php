<?php

namespace common\models;

use common\components\IdNumberValidator;
use Yii;

/**
 * This is the model class for table "{{%student}}".
 *
 * @property integer $id
 * @property string $identification_number
 * @property string $fullName
 * @property string $title_name
 * @property string $first_name
 * @property string $last_name
 * @property string $nickname
 * @property string $birthdate
 * @property string $gender
 * @property string $address
 * @property string $phone_number
 * @property string $nationality
 * @property string $race
 * @property string $religion
 * @property integer $school_grade_id
 * @property integer $academic_year_id
 * @property integer $club_id
 * @property string $created_at
 * @property string $updated_at
 * @property bool $active
 * @property float $totalScore
 *
 * @property Club[] $leadingClubs
 * @property AcademicYear $academicYear
 * @property Club $club
 * @property SchoolGrade $schoolGrade
 * @property StudentActivity[] $studentActivities
 */
class Student extends \yii\db\ActiveRecord
{
    CONST SCENARIO_CHECK_BY_ID_NUMBER = 'checkByIdNumber';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%student}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address'], 'string'],
            [['identification_number', 'title_name', 'first_name', 'last_name', 'gender'], 'required'],
            [['school_grade_id', 'academic_year_id', 'club_id'], 'required'],
            [['school_grade_id', 'academic_year_id', 'club_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['identification_number', 'title_name', 'first_name', 'last_name', 'nickname', 'gender', 'phone_number', 'nationality', 'race', 'religion'], 'string', 'max' => 255],
            ['active', 'boolean'],
            ['active', 'default', 'value' => false],
            [['academic_year_id'], 'exist', 'skipOnError' => true, 'targetClass' => AcademicYear::className(), 'targetAttribute' => ['academic_year_id' => 'id']],
            [['club_id'], 'exist', 'skipOnError' => true, 'targetClass' => Club::className(), 'targetAttribute' => ['club_id' => 'id']],
            [['school_grade_id'], 'exist', 'skipOnError' => true, 'targetClass' => SchoolGrade::className(), 'targetAttribute' => ['school_grade_id' => 'id']],
            ['club_id', function ($attribute) {
                $clubModel = Club::findOne(['id' => $this->$attribute]);
                if ($clubModel->isFull) {
                    $this->addError($attribute, 'ชุมนุมเต็มแล้ว ไม่สามารถรับคนเพิ่มเติม');
                }
            }],
            ['identification_number', IdNumberValidator::className()],
            [['birthdate'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CHECK_BY_ID_NUMBER] = ['identification_number'];
        return $scenarios;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัส',
            'identification_number' => 'เลขบัตรประชาชน',
            'fullName' => 'ชื่อเต็ม',
            'title_name' => 'คำนำหน้า',
            'first_name' => 'ชื่อ',
            'last_name' => 'สกุล',
            'gender' => 'เพศ',
            'address' => 'ที่อยู่',
            'phone_number' => 'เบอร์โทรศัพท์',
            'nationality' => 'สัญชาติ',
            'race' => 'เชื้อชาติ',
            'religion' => 'ศาสนา',
            'active' => 'สถานะได้รับการยืนยัน',
            'school_grade_id' => 'ระดับชั้น',
            'academic_year_id' => 'ปีการศึกษา',
            'club_id' => 'ชุมนุม',
            'birthdate' => 'วันเกิด',
            'nickname' => 'ชื่อเล่น',
            'created_at' => 'สร้างเมือ',
            'updated_at' => 'แก้ไขเมื่อ',
        ];
    }

    public function beforeSave($insert)
    {
        return parent::beforeSave($insert);
    }

    public function getFullName()
    {
        return "{$this->title_name}{$this->first_name} {$this->last_name}";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLeadingClubs()
    {
        return $this->hasMany(Club::className(), ['student_leader_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcademicYear()
    {
        return $this->hasOne(AcademicYear::className(), ['id' => 'academic_year_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClub()
    {
        return $this->hasOne(Club::className(), ['id' => 'club_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolGrade()
    {
        return $this->hasOne(SchoolGrade::className(), ['id' => 'school_grade_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentActivities()
    {
        return $this->hasMany(StudentActivity::className(), ['student_id' => 'id']);
    }

    public function getTotalScore()
    {
        $totalScore = 0;
        foreach ($this->studentActivities as $studentActivity) {
            $totalScore += $studentActivity->score;
        }
        return $totalScore;
    }

    /**
     * @inheritdoc
     * @return StudentQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new StudentQuery(get_called_class());
    }
}
