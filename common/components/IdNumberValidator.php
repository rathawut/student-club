<?php

/**
 * Created by PhpStorm.
 * User: ilyas
 * Date: 11/8/2016 AD
 * Time: 12:41 PM
 */

namespace common\components;

use yii\validators\Validator;

class IdNumberValidator extends Validator
{
    public function init()
    {
        parent::init();
        $this->message = 'หมายเลขบัตรประชาชนผิด';
    }

    protected function checkIdNumber($id)
    {
        if (strlen($id) != 13) {
            return false;
        }
        for ($i = 0, $sum = 0; $i < 12; $i++) {
            $sum += (int)($id{$i}) * (13 - $i);
        }
        if ((11 - ($sum % 11)) % 10 !== (int)($id{12})) {
            return false;
        }
        return true;
    }

    public function validateAttribute($model, $attribute)
    {
        if (!$this->checkIdNumber($model->$attribute)) {
            $this->addError($model, $attribute, $this->message);
        }
    }

    public function clientValidateAttribute($model, $attribute, $view)
    {
        $message = json_encode($this->message, JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        return <<<JS
function checkIDNumber(id) {
    if (id.length !== 13) {
        return false;
    }
    for (var i = 0, sum = 0; i < 12; i++) {
        sum += parseFloat(id.charAt(i)) * (13 - i);
    }
    return (11 - sum % 11) % 10 === parseFloat(id.charAt(12));
}
if (!checkIDNumber(value)) {
    messages.push($message);
}
JS;
    }
}
